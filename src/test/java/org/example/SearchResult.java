package org.example;

import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResult {
    List<WebElement> listOfElementsVendor;
    List<WebElement> listOfElementsPrices;
    List<WebElement> listOfElementsDescription;

    public SearchResult(List<WebElement> listOfElementsVendor, List<WebElement> listOfElementsPrices, List<WebElement> listOfElementsDescription) {
        this.listOfElementsVendor = listOfElementsVendor;
        this.listOfElementsPrices = listOfElementsPrices;
        this.listOfElementsDescription = listOfElementsDescription;
    }
}
