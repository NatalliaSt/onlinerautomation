package org.example;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

public class OnlinerTest {

    MainPage mainPage;
    CatalogPage catalogPage;
    private String xpathCatalog = "//span[text()='Каталог' and @class='b-main-navigation__text']/..";
    private String menuItemName = "Электроника";
    private String subMenuItemName = "Телевидение и видео";
    private String subSubMenuItemName = "Телевизоры";

    @BeforeClass
    public void setup() {
        Browser.setup();
    }

    @Test
    public void openMainPage() {
        this.mainPage = new MainPage();
        this.catalogPage = new CatalogPage();
        this.mainPage.openMainPage();
        this.mainPage.navigateSection(xpathCatalog);
        this.catalogPage.chooseMenuItem(menuItemName);
        this.catalogPage.chooseSubMenuItem(subMenuItemName);
        this.catalogPage.chooseSubSubMenuItem(subSubMenuItemName);
        FilterQuery filterQuery = new FilterQuery(ConfProperties.getProperty("FilterQueryPriceTo"),
                ConfProperties.getProperty("FilterQueryVendor"),
                ConfProperties.getProperty("FilterQueryResolution"),
                ConfProperties.getProperty("FilterQueryDiagonalMin"),
                ConfProperties.getProperty("FilterQueryDiagonalMax")
        );
        this.catalogPage.setFilter(filterQuery);
        SearchResult result = this.catalogPage.searchResult();

        result.listOfElementsVendor.forEach(item -> {
            String str = item.getText().toLowerCase(Locale.ROOT);
            assertTrue(str.contains(filterQuery.vendor));
            System.out.println(item.getText());
        });
        result.listOfElementsPrices.forEach(item -> {
            String str = item.getText();
            str = str.replace(" р.", "");
            str = str.replace(",", ".");

            float price = Float.parseFloat(str);

            int priceCoin = Math.round(price * 100);
            int toPriceCoin = Math.round(Float.parseFloat(filterQuery.priceTo) * 100);
            System.out.println(priceCoin);
            System.out.println(toPriceCoin);

            assertTrue((priceCoin < toPriceCoin));
        });
        result.listOfElementsDescription.forEach(item -> {
            System.out.println(item.getText());
            String str = item.getText().toLowerCase(Locale.ROOT);
            assertTrue(!str.contains(filterQuery.resolution));

            int indexInch = str.indexOf('"');
            if (indexInch > 0) {
                str = str.substring(0, indexInch);
                float inch = Float.parseFloat(str);
                assertTrue (inch < Float.parseFloat(filterQuery.diagonalMax) || inch > Float.parseFloat(filterQuery.diagonalMin));
            } else  {
                assertTrue(false);
            }
            System.out.println(str);

        });
    }
}
