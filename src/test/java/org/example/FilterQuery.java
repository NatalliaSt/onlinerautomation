package org.example;

public class FilterQuery {
    String priceTo;
    String vendor;
    String resolution;
    String diagonalMin;
    String diagonalMax;


    public FilterQuery(String priceTo, String vendor, String resolution, String diagonalMin, String diagonalMax) {
        this.priceTo = priceTo;
        this.vendor = vendor;
        this.resolution = resolution;
        this.diagonalMin = diagonalMin;
        this.diagonalMax = diagonalMax;
    }
}
