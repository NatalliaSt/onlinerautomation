package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;

public class MainPage {


    public MainPage() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void openMainPage() {
        Browser.driver.get(ConfProperties.getProperty("mainPageAddress"));
    }

    public void navigateSection (String xpathSection){
    WebElement section = Browser.driver.findElement(By.xpath(xpathSection));
        section.click();
    }
}
