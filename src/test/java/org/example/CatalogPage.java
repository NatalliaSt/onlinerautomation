package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CatalogPage {

    public static final int TIME_OUT_IN_SECONDS = 50;
    private String menuLocator = "//span[text()='%s']";
    private String subMenuLocator = "//div[contains(text(),'%s')]";
    private String subSubMenuLocator = "//span[contains(text(),'%s')]";
    private String xpathPriceTo = "//span[contains(text(),'Минимальная цена в предложениях магазинов')]/../following-sibling::div/descendant::input[@placeholder='до']";
    private String xpathVendor = "//div[@class='schema-filter__facet']/descendant::input[@value='%s'][1]/ancestor::label";
    private String xpathResolution = "//div[@class='schema-filter__facet']/descendant::input[@value='1920x1080'][1]/ancestor::label";
    private String xpathDiagonalSizeMin = "//select[contains(@data-bind,'facet.value.from')]";
    private String xpathDiagonalSizeMax = "//select[contains(@data-bind,'facet.value.to')]";
    private String xpathStalenessElement = "//div[@class='schema-product__group'][1]";
    private String xpathProductDescription = "//span[contains(@data-bind,'product.description')]";
    private String xpathDataPrices = "//span[contains(@data-bind,'data.prices')]";
    private String xpathProductFullName = "//span[@data-bind='html: product.extended_name || product.full_name']";

    public void chooseMenuItem(String item) {
        menuSelectElement(item).click();
    }

    private WebElement menuSelectElement(String text) {
        System.out.print(String.format(menuLocator, text));
        return Browser.driver.findElement(By.xpath(String.format(menuLocator, text)));
    }

    private WebElement subMenuSelectElement(String text) {
        System.out.print(String.format(subMenuLocator, text));
        return Browser.driver.findElement(By.xpath(String.format(subMenuLocator, text)));
    }

    private WebElement subSubMenuSelectElement(String text) {
        System.out.print(String.format(subMenuLocator, text));
        return Browser.driver.findElement(By.xpath(String.format(subSubMenuLocator, text)));
    }

    public void chooseSubMenuItem(String item) {
        subMenuSelectElement(item).click();
    }

    public void chooseSubSubMenuItem(String item) {
        subSubMenuSelectElement(item).click();
    }

    public void setFilter(FilterQuery filterQuery)  {

        WebElement priceTo = Browser.driver.findElement(By.xpath(xpathPriceTo));
        WebElement samsung = Browser.driver.findElement(By.xpath(String.format(xpathVendor, filterQuery.vendor)));
        WebElement fullHd = Browser.driver.findElement(By.xpath(xpathResolution));
        WebElement sizeMin = Browser.driver.findElement(By.xpath(xpathDiagonalSizeMin));
        WebElement sizeMax = Browser.driver.findElement(By.xpath(xpathDiagonalSizeMax));

        JavascriptExecutor jse = (JavascriptExecutor) Browser.driver;


        jse.executeScript("scroll(0, 1250)");
        fullHd.click();

        priceTo.sendKeys(filterQuery.priceTo);

        new Select(sizeMin).selectByVisibleText(String.format("%s\"", filterQuery.diagonalMin));

        new Select(sizeMax).selectByVisibleText(String.format("%s\"", filterQuery.diagonalMax));

        jse.executeScript("scroll(0, 1250)");
        samsung.click();
        WebDriverWait wait = new WebDriverWait(Browser.driver, TIME_OUT_IN_SECONDS);
        WebElement elementOldPage = Browser.driver.findElement(By.xpath(xpathStalenessElement));
        wait.until(ExpectedConditions.stalenessOf(elementOldPage));
    }



    public SearchResult searchResult () {
        List<WebElement> listOfElementsVendor = Browser.driver.findElements(By.xpath(xpathProductFullName));
        List<WebElement> listOfElementsPrices = Browser.driver.findElements(By.xpath(xpathDataPrices));
        List<WebElement> listOfElementsDescription = Browser.driver.findElements(By.xpath(xpathProductDescription));
        return new SearchResult(listOfElementsVendor, listOfElementsPrices, listOfElementsDescription);
    }
}
